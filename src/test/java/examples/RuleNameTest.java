package examples;

import org.testng.annotations.Test;
import sample.RuleName;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by pawelbrataniec on 05/04/17.
 */
public class RuleNameTest {

    @Test
    public void shouldCreateRuleEnum() {

        RuleName unique = RuleName.getRulenameFromString("unique");
        assertThat(unique).isEqualTo(RuleName.unique);

    }
}