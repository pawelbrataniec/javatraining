package sample;

import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

/**
 * Created by pawelbrataniec on 10/04/17.
 */
public class RuleNameFactoryTest {

    RuleFactory factory = new RuleNameFactory();

    @Test
    public void shouldCreateProperRuleBasedOnRuleName() {
        RuleName ruleName = RuleName.getRulenameFromString("mostFrequent");

        Rule rule = factory.getRule(ruleName);

        assertTrue(rule instanceof MostFrequentRule);
        assertTrue(factory.getRule(RuleName.unique) instanceof UniquRule);
    }

    @Test(expectedExceptions = RuntimeException.class)
    public void shouldThrowSomeExceptionWhenCanNotCreateRule() {
        factory.getRule(RuleName.undefined);
    }
}