package sample;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by pawelbrataniec on 04/04/17.
 */
public class UniquRuleTest {

    Rule rule = new UniquRule();

    @Test
    public void shouldReturnEmptyWhenInputIsEmpty() {
        assertThat(rule.process("")).isEqualTo("");
    }

    @Test
    public void shouldReturnPropperNumberOfUniqueWords() {
        assertThat(rule.process("Ala ma kota")).isEqualTo("3");
    }

    @Test
    public void shouldReturnEmptyWhenInputIsNull() {
        assertThat(rule.process("")).isEqualTo("");
    }

    @Test
    public void shouldReturnPropperNumberOfUniqueWordsWhenInInputIsMoreSpaces() {
        assertThat(rule.process(" Ala ma kota ")).isEqualTo("3");
    }

}