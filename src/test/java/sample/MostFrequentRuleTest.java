package sample;

import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by pawelbrataniec on 06/04/17.
 */
public class MostFrequentRuleTest {

    Rule rule = new MostFrequentRule();

    @Test
    public void shouldReturnEmptyStringWhenInputIsEmpty() {
        assertThat(rule.process("")).isEmpty();
    }

    @Test
    public void shouldReturnPropperValueWhenInputWillBeNotEmpty() {
        assertThat(rule.process("Ala Ala ma kota")).isEqualTo("Ala");
    }

    @Test
    public void shouldReturnEmptyValueWhenInputHasNoMax() {
        assertThat(rule.process("Ala ma kota")).isEqualTo("");
    }

    @Test
    public void shouldReturnEmptyValueWhenInputHasNoMax2() {
        assertThat(rule.process("Ala Ala ma ma kota")).isEqualTo("");
    }
}