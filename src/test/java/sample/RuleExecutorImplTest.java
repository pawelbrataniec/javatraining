package sample;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by pawelbrataniec on 10/04/17.
 */
public class RuleExecutorImplTest {

    RuleExecutor executor;

    @BeforeMethod
    public void setUp() {
        executor = new RuleExecutorImpl("ala ma kota");
    }

    @Test
    public void shouldExecuteRule() {
        assertThat(executor.execute(new UniquRule())).isEqualTo("3");
    }

    @Test
    public void shouldCreateRuleFromFactoryAndExecuteThem() {
        RuleFactory factory = new RuleNameFactory();
        assertThat(executor.execute(factory.getRule(RuleName.mostFrequent))).isEqualTo("");
    }
}