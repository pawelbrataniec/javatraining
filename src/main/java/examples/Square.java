package examples;

/**
 * Created by pawelbrataniec on 05/04/17.
 */
public class Square extends Figure {


    public Square(String color) {
        super(color);
    }

    @Override
    public int getNUmberOfSides() {
        return 4;
    }
}
