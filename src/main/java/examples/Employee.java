package examples;

/**
 * Created by pawelbrataniec on 05/04/17.
 */
public class Employee {

    protected String imie;
    protected double salary;

    public Employee() {
        System.out.println("Employee");
    }

    public Employee(String imie, double salary) {
        this.imie = imie;
        this.salary = salary;
        System.out.println("Employee");
    }

    public double getSalary() {
        return salary;
    }
}
