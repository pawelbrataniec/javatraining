package examples;

/**
 * Created by pawelbrataniec on 05/04/17.
 */
public class Singleton {

    private static Singleton INSTANCE = null;

    private Singleton() {
    }

    private static Singleton getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new Singleton();
        }
        return INSTANCE;
    }
}
