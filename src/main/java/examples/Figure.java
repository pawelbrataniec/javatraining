package examples;

/**
 * Created by pawelbrataniec on 05/04/17.
 */
public abstract class Figure {

    protected String color;

    public Figure(String color) {
        this.color = color;
    }

    public String getColor(){
        return color;
    };

    public abstract int getNUmberOfSides();

}
