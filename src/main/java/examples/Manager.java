package examples;

/**
 * Created by pawelbrataniec on 05/04/17.
 */
public class Manager extends Employee {

    private double bonus;

    public Manager() {
        System.out.println("Manager");
    }

    public Manager(String imie, double salary, double bonus) {
        super(imie, salary);
        this.bonus = bonus;
        System.out.println("Manager");
    }

    @Override
    public double getSalary() {
        return salary + bonus;
    }
}
