package sample;

/**
 * Created by pawelbrataniec on 10/04/17.
 */
public class RuleNameFactory implements RuleFactory {
    @Override
    public Rule getRule(RuleName name) {

        switch (name) {
            case mostFrequent:
                return new MostFrequentRule();
            case unique:
                return new UniquRule();
            default:
                throw new RuntimeException("Can not create rule from ruleName =" + name.name());
        }
    }
}
