package sample;

/**
 * Created by pawelbrataniec on 05/04/17.
 */
public enum RuleName {

    unique("unique"), mostFrequent("mostFrequent"), undefined("");

    private String name;

    RuleName(String name) {
        this.name = name;
    }

    public static RuleName getRulenameFromString(String name) {
        for (RuleName ruleName : values()) {
            if (name.equals(ruleName.name)) {
                return ruleName;
            }
        }
        throw new RuntimeException("Can not find rule with name " + name);
    }
}
