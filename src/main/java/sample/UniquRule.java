package sample;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by pawelbrataniec on 04/04/17.
 */
public class UniquRule implements Rule {
    public String process(String input) {
        return Optional.ofNullable(input)
                .filter(s -> !s.isEmpty())
                .map(s ->
                        String.valueOf(Arrays.asList(input.trim().split(" ")).stream()
                                .distinct()
                                .collect(Collectors.toList())
                                .size())
                ).orElse("");
    }
}
