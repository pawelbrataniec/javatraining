package sample;

/**
 * Created by pawelbrataniec on 04/04/17.
 */
public interface Rule {

    String process(String input);
}
