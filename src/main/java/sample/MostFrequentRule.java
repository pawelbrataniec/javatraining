package sample;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by pawelbrataniec on 06/04/17.
 */
public class MostFrequentRule implements Rule {
    @Override
    public String process(String input) {
        return Optional.ofNullable(input)
                .filter(s -> !s.isEmpty())
                .map(s -> {
                    List<String> strings = Arrays.asList(s.trim().split(" "));

                    Map<String, Long> collect = strings.stream().collect(Collectors.groupingBy(o -> o, Collectors.counting()));

                    long count = collect.entrySet().stream().filter(stringLongEntry -> stringLongEntry.getValue() > 1).count();

                    if (count > 1) {
                        return "";
                    }

                    Optional<Map.Entry<String, Long>> max = collect.entrySet().stream().max((o1, o2) -> Long.compare(o1.getValue(), o2.getValue()));

                    return max.filter(stringLongEntry -> stringLongEntry.getValue() != 1)
                            .map(stringLongEntry -> stringLongEntry.getKey())
                            .orElse("");
                }).orElse("");
    }
}
