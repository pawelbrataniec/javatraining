package sample;

/**
 * Created by pawelbrataniec on 10/04/17.
 */
public class RuleExecutorImpl implements RuleExecutor {

    private String input;

    public RuleExecutorImpl(String input) {
        this.input = input;
    }

    @Override
    public String execute(Rule rule) {
        return rule.process(input);
    }
}
