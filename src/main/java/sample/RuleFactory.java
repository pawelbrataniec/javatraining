package sample;

/**
 * Created by pawelbrataniec on 10/04/17.
 */
public interface RuleFactory {

    Rule getRule(RuleName name);
}
